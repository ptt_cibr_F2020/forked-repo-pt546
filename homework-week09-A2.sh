#!/bin/bash
# ==============================================================================
#
# FILE: homework-week09-A2.sh
#   
# DESCRIPTION: Contains git commands and answers to Assignment 2 Q's 
#
# OPTIONS: see comments below
# REQUIREMENTS: this script
# BUGS: ---
# NOTES: Markdown cheat sheet: https://www.markdownguide.org/cheat-sheet/
#
# AUTHOR: Paulina Tolosa Tort
# COMPANY: Yale University
# CREATED: 2020-11-10
#
# ==============================================================================

#### Week 09 Assignment 2 ####
##
# Q1
git clone git@bitbucket.org:ptt_cibr_F2020/forked-repo-pt546.git

# Q2
git commit --amend -m 'Last commit for'

# Q3
git rebase -i HEAD~4
##-- substitute "pick" for "reword" in the file that is opened by the rebase command
##-- save changes with :wq
##-- the selected commit will open; edit woth "pt546"
git log # to verify

# Q4
git branch development

# Q5
git checkout development
touch dev-week09.txt
now=$(date)
echo "$now" > dev-week09.txt
git commit -m 'docs: A2Q5: created dev-week09.txt that echos current time'

# Q6
git tag -a '0.1.0' -m 'initial commit'
git push origin --tag

# Q7
git log
git checkout a97e149edc78fe0631c1e9b08ebc2ac318fe2d73

# Q8
git checkout -b release/1.0.0
git checkout master
git merge release/1.0.0
vim Readme.md
git add .
git commit -m 'docs: updated README'
git push

# Q9
git branch -d release/1.0.0

# Q10
git merge development

# Q11
git submodule add git@bitbucket.org:ptt_cibr_F2020/cibr-student-pt546.git
git submodule init
git commit -m 'chore: created a submodule of cibr_student_pt546'
git push

# Q12
git fsck --full

# Q13
# revert undoes a previous commit by creating a new commit but does not chenge the commit history
# reset changes the commit history

# Q14
# hotfix branches are created directly from the master branch to fix bugs that were not detected during testing and have been publocly released.

# Q15 hotfix branches should merge to master to fix the publicly available branch but also back to development to keep it updated.
